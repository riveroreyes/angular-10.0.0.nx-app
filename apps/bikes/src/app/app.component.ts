import { Component } from '@angular/core';

@Component({
  selector: 'rentals-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'bikes';

  items = [
    {
      brand: 'Shimano',
      variant: 'W2',
      price: 3.5
    },
    {
      brand: 'Saguamura',
      variant: 'Z1',
      price: 11.2
    },
  ];
}
