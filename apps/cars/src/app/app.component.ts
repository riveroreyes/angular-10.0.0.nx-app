import { Component } from '@angular/core';

@Component({
  selector: 'rentals-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'cars';

  items = [
    {
      brand: 'Mercedes',
      variant: 'A1',
      price: 20
    },
    {
      brand: 'Fiat',
      variant: 'A3',
      price: 15
    },
  ];
}
